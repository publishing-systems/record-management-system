<?php
/* Copyright (C) 2014-2024 Stephan Kreutzer
 *
 * This file is part of record-management-system.
 *
 * record-management-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * record-management-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with record-management-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/lang/en/language_selector.lang.php
 * @author Stephan Kreutzer
 * @since 2014-05-31
 */



define("LANG_LANGUAGESELECTOR_DESCRIPTION", "Select a language: ");
define("LANG_LANGUAGESELECTOR_SUBMITBUTTON", "Apply");



?>
